{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Config 
  ( PGConfig(..)
  , getPGConfig
  , PastorConfig(..)
  , getConfig
  ) where

import GHC.Generics

import qualified Data.ByteString as B

import Data.Word

import Data.Maybe

import System.IO.Unsafe

import qualified System.Envy as Env

data PGConfig = PGConfig 
  { pastorPgHost :: B.ByteString
  , pastorPgPort :: Word16
  , pastorPgUser :: B.ByteString
  , pastorPgPass :: B.ByteString
  , pastorPgDb   :: B.ByteString
  } deriving (Generic, Show)

-- Default instance used if environment variable doesn't exist
instance Env.DefConfig PGConfig where
  defConfig = PGConfig "localhost" 5432 "pastor" "pastor" "pastor"
instance Env.FromEnv PGConfig

getPGConfig :: PGConfig
getPGConfig = fromJust $ unsafePerformIO (Env.decode :: IO (Maybe PGConfig))


data PastorConfig = PastorConfig
  { pastorPort :: Int
  , pastorLog  :: String
  , pastorDataDir :: String
  } deriving (Generic, Show)

instance Env.DefConfig PastorConfig where
  defConfig = PastorConfig 3000 "DEBUG" "/var/lib/pastor"
instance Env.FromEnv PastorConfig

getConfig :: PastorConfig
getConfig = fromJust $ unsafePerformIO (Env.decode :: IO (Maybe PastorConfig))
